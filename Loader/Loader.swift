//
//  Loader.swift
//  Loader
//
//  Created by Maciek Gierszewski on 12/04/2018.
//  Copyright © 2018 Future Mind sp. z o. o. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class Loader: UIView {
    private static let size = CGSize(width: 20.0, height: 20.0)
    
    private func setNeedsUpdateImage() -> Void {
        imageView.image = nil
        setNeedsLayout()
    }
    
    private func createImage() -> UIImage? {
        let size = intrinsicContentSize
        let center = CGPoint(x: size.width/2.0, y: size.height/2.0)
        
        let outerRadius = size.width/2.0 - 1.0
        let innerRadius = outerRadius - segmentHeight
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        for i in 0..<segmentCount {
            let progress = CGFloat(i)/CGFloat(segmentCount)
            
            let color = UIColor(white: 0.0, alpha: progress)
            color.setStroke()
            
            let angle = CGFloat.pi * 2.0 * progress
            
            let bezierPath = UIBezierPath()
            bezierPath.lineCapStyle = segmentCapStyle
            bezierPath.lineWidth = segmentWidth
            
            bezierPath.move(to: CGPoint(x: center.x - sin(angle) * innerRadius, y: center.y + cos(angle) * innerRadius))
            bezierPath.addLine(to: CGPoint(x: center.x - sin(angle) * outerRadius, y: center.y + cos(angle) * outerRadius))
            bezierPath.stroke()
        }
        let returnImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return returnImage?.withRenderingMode(.alwaysTemplate)
    }
    
    private func updateVisibility() -> Void {
        isHidden = hidesWhenStopped && (animating == false)
    }
    
    @objc private func tick(_ displayLink: CADisplayLink) -> Void {
        let timestamp = displayLink.timestamp
        let roundedTimestamp = floor(timestamp)
        
        let continuousProgress = timestamp - roundedTimestamp
        let quantifiedProgress = round(continuousProgress * Double(segmentCount)) / Double(segmentCount)
        
        imageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 2.0 * CGFloat(quantifiedProgress))
    }
    
    private var displayLink: CADisplayLink? {
        willSet {
            displayLink?.remove(from: .current, forMode: .commonModes)
        }
        didSet {
            displayLink?.add(to: .current, forMode: .commonModes)
        }
    }
    
    private var imageView = UIImageView()
    
    var hidesWhenStopped: Bool = true {
        didSet {
            updateVisibility()
        }
    }
    
    // MARK: appearance
    
    @objc dynamic var segmentCount: Int = 90 {
        didSet { setNeedsUpdateImage() }
    }
    
    @objc dynamic var segmentHeight: CGFloat = 3.0 {
        didSet { setNeedsUpdateImage() }
    }
    
    @objc dynamic var segmentWidth: CGFloat = 0.5 {
        didSet { setNeedsUpdateImage() }
    }
    
    @objc dynamic var segmentCapStyle: CGLineCap = .round {
        didSet { setNeedsUpdateImage() }
    }
    
    @objc dynamic var color: UIColor = .black {
        didSet { imageView.tintColor = color }
    }
    
    // MARK: animation
    
    var animating: Bool = false {
        didSet {
            if animating {
                displayLink = CADisplayLink(target: self, selector: #selector(tick(_:)))
            } else {
                displayLink = nil
            }
            updateVisibility()
        }
    }
    
    func startAnimating() -> Void {
        animating = true
    }
    
    func stopAnimating() -> Void {
        animating = false
    }
    
    // MARK: init
    
    init() {
        super.init(frame: CGRect(origin: .zero, size: Loader.size))
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: view hierarchy and layout
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setNeedsUpdateImage()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        updateVisibility()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if imageView.superview == nil {
            imageView.clipsToBounds = false
            addSubview(imageView)
        }
        
        if imageView.image == nil {
            imageView.tintColor = color
            imageView.image = createImage()
        }
        
        imageView.bounds = CGRect(origin: .zero, size: intrinsicContentSize)
        imageView.center = CGPoint(x: frame.width/2.0, y: frame.height/2.0)
    }
    
    override var intrinsicContentSize: CGSize {
        return Loader.size
    }
}
