//
//  ViewController.swift
//  Loader
//
//  Created by Maciek Gierszewski on 12/04/2018.
//  Copyright © 2018 Future Mind sp. z o. o. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var loader1: Loader?
    @IBOutlet var loader2: Loader?
    @IBOutlet var loader3: Loader?
    
    @IBOutlet var button1: UIButton?
    @IBOutlet var button2: UIButton?
    @IBOutlet var button3: UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader1?.color = .blue
        loader2?.color = .purple
        loader3?.color = .red
    }
    
    @IBAction func toggle(_ button: UIButton) -> Void {
        let loader: Loader?
        if button == button1 {
            loader = loader1
        } else if button == button2 {
            loader = loader2
        } else {
            loader = loader3
        }
        
        if let animating = loader?.animating, animating == false {
            button.setTitle("stop", for: .normal)
            loader?.startAnimating()
        } else {
            button.setTitle("start", for: .normal)
            loader?.stopAnimating()
        }
    }
}

